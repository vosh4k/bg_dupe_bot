## Setup
```
virtualenv venv
. venv/bin/activate

pip install -U pip
pip install -r requirements.txt
```

Add your telegram bot token to `bot/settings.py` as `TOKEN`
```python
# bot/settings.py
TOKEN = '83242423:KLhjy89JnBG7678iJght6AS9'
```

## Run
```
python run.py
```

## Add messages
Add them to the lists in `bot/messages.py`
