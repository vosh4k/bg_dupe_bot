import logging

from bot.dupe import DupeBot

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)
    logging.info('Bot listening...')
    dupe = DupeBot()
    dupe.main()
