import hashlib
from datetime import datetime, timedelta
from random import choice

from telegram.ext import Updater, RegexHandler

import bot
from bot import messages
from bot.database import History


class DupeBot:
    def __init__(self):
        self.db = History()

    def handle_message(self, bot, update):
        sha1 = hashlib.sha1(str(update.message.text).encode('utf-8')).hexdigest()
        db_row = self.db.get_if_exists(update.message.chat_id, sha1)

        if not db_row:
            self.db.save(update.message.chat_id, update.effective_user.name, sha1)
            return

        timestamp = datetime.strptime(db_row['timestamp'], '%Y-%m-%d %H:%M:%S')
        now = datetime.utcnow()

        # if today
        if timestamp.date() == now.date():
            message = choice(messages.TODAY)

        # if yesterday
        elif timestamp.date() == (now - timedelta(days=1)).date():
            message = choice(messages.YESTERDAY)

        # if ever
        else:
            message = messages.DEFAULT

        bot.send_message(chat_id=update.message.chat_id, text=message)

    def main(self):
        updater = Updater(token=bot.TOKEN)
        handler = RegexHandler(bot.MESSAGE_REGEX, self.handle_message)
        updater.dispatcher.add_handler(handler)
        updater.start_polling()
